# API Template

This repo is a template for WebApp and API development. It uses Flask for routing, Jinja2 for templating, and assumes some form of SQL-like database for a backend in its structure. 

### The Folder Structure:
---

- App
	- Models: Organizes the SQL statements for use with the API.
	- Static: A flask-linked folder that holds CSS/JS/assets for display.
	- Templates: These are the Jinja2 html templates.
	- Views: This is the router for organizing how flask connects the templates.
- Tests 
	- API: This holds the functionality for your developed application. Simply import from app, tests are templated to run in PyTest.
	- DB: This tests the basic database functionality for the application. It does not support generic SQL-alchemy or migrations.
- Misc. Others
	- manager.py: How you interact and start the app.
		- To run the application (executes App/app.py - createApp):
```python
python manager.py
```
		- To seed the database (executes App/app.py - doDB): 
```python
python manager.py --seed
```
	- requirements.txt: Organizes all of the default packages used in the template.
	- config.py: For additional configuration (and multiple configuration) at the app level.