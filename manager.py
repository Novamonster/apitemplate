import argparse
import os
import sys

from app import createApp, doDB

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Simple controller for flask app.')
    parser.add_argument('--seed', action='store_true',
                        help='Execute the seeding of MariaDB - creates a database, table, and fills it with sample data.')
    parser.add_argument('--configuration', action='store', default="standard",
                        help='Creates Flask app with the specified configuration from (config.py).')
    args = parser.parse_args()
    if args.seed:
        doDB()
        sys.exit(0)

    app = createApp(this_conf=args.configuration)
    app.run(host="localhost", port=3000)
