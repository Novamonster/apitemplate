import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY')
    RESULTS_PER_API_CALL = 25
    STATIC_FOLDER = 'app/static'

    @staticmethod
    def init_app(app):
        '''
            If some configuration needs to initialize the app in some way use this function
            :param app: Flask app
            :return:
        '''
        pass


class ProductionConfig(Config):
    DATABASE_URI = os.environ.get('PROD_DATABASE_URL')


configuration = {
    'production': ProductionConfig,
    'standard': Config,

}
